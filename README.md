# Project FPGA Synthesizer

## What is this
This is the source code for the final project for class ECE 385 at University of Illinois at Urbana-Champaign by the group consisting of Tianshu Wei and Nuoyan Wang. 
## Functionality
A MIDI file (with multiple concurrent notes allowed) is converted by python MIDI libraries and our own logical functions into a valid 2D array of (durations, pitch) pairs. Next, our Sound Module in Quartus will generate  square waves at the specified 'pitch' frequency for the specified 'duration,' which are output to our FPGA's GPIO pins to varying devices.

Furthermore, triangle waves can be generated and wired to a physical circuit outside to create a more retro arcade sound texture.

In the current state, we have currently used a MIDI file of "River Flows in You" by Yiruma, with up to 4 concurrent notes playing at once. 

## How to run the code
The files in the root folder mostly are used by Intel Quartus which will generate a SoC consisting of 4 sound modules and 2 drum modules and 1 LED module. The code in software folder consist of the code run by the NIOS II e CPU that is included in the generated SoC and is accountable for the functionality of the instrument.

